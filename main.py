'''
import PIL
from PIL import Image
import wifi_qrcode_generator
img = wifi_qrcode_generator.wifi_qrcode(
	'Like Pi - Devices', False, 'WPA', '12345678',
	box_size=15,
	version=1,
	border=0,
)


if(img.size[0] != 555):
    img = img.resize( (555,555), PIL.Image.ANTIALIAS)
img = img.convert(mode="RGB")

logo=Image.open("logo.png")
img.paste(logo, (185,185))
img.save('qrcode.png')
img.show()

exit()

'''


import qrcode
import PIL
from PIL import Image


ssdi = 'Like Pi - Devices'
senha = 'FCC4C310'

qr = qrcode.QRCode(
	version=1,
	error_correction=qrcode.constants.ERROR_CORRECT_H,
	box_size=15,
	border=0,
)
qr.add_data('WIFI:S:'+ssdi+';T:WPA;P:'+senha+';;')
qr.make(fit=True)

img = qr.make_image()
if(img.size[0] != 555):
	img = img.resize( (555,555), PIL.Image.ANTIALIAS)
img = img.convert(mode="RGB")

logo=Image.open("logo.png")
print(logo)

img.paste(logo, (185,185))
img.save("qrcodeteste.png")
